package pcm.uevora;

import pcm.uevora.note.NoteEdit;
import pcm.uevora.note.NotesDbAdapter;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class VerListas extends ListActivity {
	
	private ListasDbAdapter mDbHelper;
	
	private static final int ACTIVITY_PRODUTOS=0;
	
	private static final int DELETE_ID = Menu.FIRST;

		public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.listasguardadas);
	        mDbHelper = new ListasDbAdapter(this);
	        mDbHelper.open();
	        fillData();
	        registerForContextMenu(getListView());
		}
	    
	    private void fillData() {
	        Cursor listasCursor = mDbHelper.fetchAllListas();
	        startManagingCursor(listasCursor);
	        
	        // Create an array to specify the fields we want to display in the list (only TITLE)
	        String[] from = new String[]{ListasDbAdapter.KEY_TITLE};
	        
	        // and an array of the fields we want to bind those fields to (in this case just text1)
	        int[] to = new int[]{R.id.text1};
	        
	        // Now create a simple cursor adapter and set it to display
	        SimpleCursorAdapter listas = 
	        	    new SimpleCursorAdapter(this, R.layout.listas_row, listasCursor, from, to);
	        setListAdapter(listas);
	    }
	    
	    /*@Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        super.onCreateOptionsMenu(menu);
	        //menu.add(0, INSERT_ID, 0, R.string.menu_insert); //mudar isto!
	        return true;
	    }

	    @Override
	    public boolean onMenuItemSelected(int featureId, MenuItem item) {
	        switch(item.getItemId()) {
	        case INSERT_ID:
	            createLista();
	            return true;
	        }
	       
	        return super.onMenuItemSelected(featureId, item);
	    }*/
		
	    @Override
		public void onCreateContextMenu(ContextMenu menu, View v,
				ContextMenuInfo menuInfo) {
			super.onCreateContextMenu(menu, v, menuInfo);
	        menu.add(0, DELETE_ID, 0, R.string.menu_delete_list);
		}

	    @Override
		public boolean onContextItemSelected(MenuItem item) {
			switch(item.getItemId()) {
	    	case DELETE_ID:
	    		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		        mDbHelper.deleteLista(info.id);
		        fillData();
		        return true;
			}
			return super.onContextItemSelected(item);
		}
		
	    private void verProdutos() {
	        Intent i = new Intent(this, VerProdutos.class);
	        
	        startActivityForResult(i, ACTIVITY_PRODUTOS);
	    }
	    
	    @Override
	    protected void onListItemClick(ListView l, View v, int position, long id) {
	        super.onListItemClick(l, v, position, id);
	        Intent i = new Intent(this, VerProdutos.class);
	        i.putExtra(ListasDbAdapter.KEY_ROWID, id);
	        mDbHelper.close();
	        startActivityForResult(i, ACTIVITY_PRODUTOS);
	    }

	    @Override
	    protected void onActivityResult(int requestCode, int resultCode, 
	                                    Intent intent) {
	        super.onActivityResult(requestCode, resultCode, intent);
	        mDbHelper.open();
	        fillData();
	    }
	 

}
