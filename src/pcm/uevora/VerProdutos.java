package pcm.uevora;

	import pcm.uevora.note.NoteEdit;
	import pcm.uevora.note.NotesDbAdapter;
	import android.app.ListActivity;
	import android.content.Intent;
	import android.database.Cursor;
	import android.os.Bundle;
	import android.view.ContextMenu;
	import android.view.Menu;
	import android.view.MenuItem;
	import android.view.View;
	import android.view.ContextMenu.ContextMenuInfo;
	import android.widget.ListView;
	import android.widget.SimpleCursorAdapter;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class VerProdutos extends ListActivity {


	private ListasDbAdapter mDbHelper;
	
	 private Long mRowId;

		public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.listasguardadas);
	        mDbHelper = new ListasDbAdapter(this);
	        mDbHelper.open();
	        
	        if (mRowId == null) {
				Bundle extras = getIntent().getExtras();            
				mRowId = extras != null ? extras.getLong(ListasDbAdapter.KEY_ROWID) 
										: null;
			}
	        fillData();
		}
	    
	    private void fillData() {
	        Cursor listasCursor = mDbHelper.fetchLista(mRowId);
	        startManagingCursor(listasCursor);
	        
	        // Create an array to specify the fields we want to display in the list (only TITLE)
	        String[] from = new String[]{ListasDbAdapter.KEY_TITLE};
	        
	        // and an array of the fields we want to bind those fields to (in this case just text1)
	        int[] to = new int[]{R.id.text1};
	        
	        // Now create a simple cursor adapter and set it to display
	        SimpleCursorAdapter listas = 
	        	    new SimpleCursorAdapter(this, R.layout.listas_row, listasCursor, from, to);
	        setListAdapter(listas);
	    }

	}



