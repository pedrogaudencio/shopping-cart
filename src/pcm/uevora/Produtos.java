package pcm.uevora;


import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Produtos extends ListActivity {
    /** Called when the activity is first created. */
	
	private CheckBoxifiedTextListAdapter check;
	// Criar CheckBox List Adapter
	
	private String[] arr;
	// Array of string we want to display in our list
	
	Button btcancel;
	Button btok;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.mainlist);
        
        btcancel = (Button) findViewById(R.id.btcancel);
        btok = (Button) findViewById(R.id.btok);
        
        
        
        Bundle extras = getIntent().getExtras();
        int decisao = extras.getInt("opcao");
        
			check = new CheckBoxifiedTextListAdapter(this);
			
			setListeners();
        
			if(decisao==1){
				arr = getResources().getStringArray(R.array.bebidas);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			
			else
				if(decisao==2){
				arr=  getResources().getStringArray(R.array.charcutaria);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==3){
				arr=  getResources().getStringArray(R.array.condimentos);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==4){
				arr=  getResources().getStringArray(R.array.congelados);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==5){
				arr=  getResources().getStringArray(R.array.frutas);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==6){
				arr=  getResources().getStringArray(R.array.higiene);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==7){
				arr=  getResources().getStringArray(R.array.lacticinios);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==8){
				arr=  getResources().getStringArray(R.array.legumes);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==9){
				arr=  getResources().getStringArray(R.array.limpeza);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==10){
				arr=  getResources().getStringArray(R.array.mercearia);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==11){
				arr=  getResources().getStringArray(R.array.padaria);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==12){
				arr=  getResources().getStringArray(R.array.papelaria);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==13){
				arr=  getResources().getStringArray(R.array.peixaria);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==14){
				arr=  getResources().getStringArray(R.array.talho);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
			else
				if(decisao==15){
				arr=  getResources().getStringArray(R.array.utilidades);
           		for(int i=0; i<arr.length; i++) {check.addItem(new CheckBoxifiedText(arr[i], false));}
				setListAdapter(check);
			}
    }
    
    public void setListeners(){
    	
    	btcancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(-2);
        	    finish();
			}
		});
    	
    	btok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = getIntent();
				
				String [] temp = new String [check.getCount()];
				
				int c=0;
				
				for(int b=0;b<check.getCount();b++){
					if(((CheckBoxifiedText)(check.getItem(b))).getChecked()==true){
						temp[c] = new String(((CheckBoxifiedText)(check.getItem(b))).getText());
						c++;
					}
				}
				
				String [] temp2 = new String [c];
				
				for(int b=0;b<c;b++)
					temp2[b] = new String(temp[b]);
				
				Bundle b = new Bundle();
				b.putStringArray("PRODUTOS", temp2);
				
				i.putExtras(b);
				setResult(RESULT_OK,i);
        	    finish();
			}
		});
   
    }
    
    
    
}
