/**
 * 
 */
package pcm.uevora;

import java.util.Arrays;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

/**
 * @author Seph
 *
 */
public class ListarProdutos extends Activity {
	
	String [] artigos = null;
	
	private static final int ACTIVITY_LISTARARTIGOS=0;
	
	Button btBebidas;
	Button btCharcutaria;
	Button btCondimentos;
	Button btCongelados;
	Button btFrutas;
	Button btHigiene;
	Button btLacticinios;
	Button btleghort;
	Button btLimpeza;
	Button btmercearia;
	Button btpadaria;
	Button btpapelaria;
	Button btpeixaria;
	Button bttalho;
	Button btutilidades;
	
	Button btcancelgerar;
	Button btokgerar;
	
	Context ctx;
	
	
	public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gerarlista);
        
        ctx = this;
        
        btBebidas = (Button) findViewById(R.id.btbebidas);
    	btCharcutaria = (Button) findViewById(R.id.btcharcutaria);
    	btCondimentos = (Button) findViewById(R.id.btcondimentos);
    	btCongelados = (Button) findViewById(R.id.btcongelados);
    	btFrutas = (Button) findViewById(R.id.btfruta);
    	btHigiene = (Button) findViewById(R.id.bthigiene);
    	btLacticinios = (Button) findViewById(R.id.btlacticinios);
    	btleghort = (Button) findViewById(R.id.btleghort);
    	btLimpeza = (Button) findViewById(R.id.btlimpeza);
    	btmercearia = (Button) findViewById(R.id.btmercearia);
    	btpadaria = (Button) findViewById(R.id.btpadaria);
    	btpapelaria = (Button) findViewById(R.id.btpapelaria);
    	btpeixaria = (Button) findViewById(R.id.btpeixaria);
    	bttalho = (Button) findViewById(R.id.bttalho);
    	btutilidades = (Button) findViewById(R.id.btutilidades);
    	
    	btcancelgerar = (Button) findViewById(R.id.btcancelgerar);
    	btokgerar = (Button) findViewById(R.id.btokgerar);
    	
    	setListeners();
    
	}
	
	public void setListeners(){
		
		btBebidas.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 1);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btCharcutaria.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 2);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btCondimentos.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 3);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btCongelados.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 4);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btFrutas.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 5);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btHigiene.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 6);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btLacticinios.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 7);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btleghort.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 8);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btLimpeza.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 9);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btmercearia.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 10);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btpadaria.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 11);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btpapelaria.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 12);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btpeixaria.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 13);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		bttalho.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 14);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		btutilidades.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Produtos.class);
				i.putExtra("opcao", 15);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
			}
		});
		
		btcancelgerar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(-2);
        	    finish();
			}
		});
		
    	btokgerar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_OK);
				
				Intent i = getIntent();
				Bundle b = new Bundle();
				b.putStringArray("PRODUTOS", artigos);
				
				i.putExtras(b);
				setResult(RESULT_OK,i);
        	    finish();
			}
		});
   
    }
	
    @Override
    protected void onActivityResult(int requestCode, int resultCode, 
                                    Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        
        String [] temp = null;
        
        if(resultCode == RESULT_OK){
        	Bundle b = intent.getExtras();
        	temp = b.getStringArray("PRODUTOS");
        	if (artigos == null)
        		artigos = temp;
        	else
        		artigos = concat(temp, artigos);
        }
    }
        
    public String[] concat(String[] A, String[] B) {
    	String[] C= new String[A.length+B.length];
    	System.arraycopy(A, 0, C, 0, A.length);
    	System.arraycopy(B, 0, C, A.length, B.length);

    	return C;
    }
    

}
