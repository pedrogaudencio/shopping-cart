package pcm.uevora;

import pcm.uevora.note.Notepadv3;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class Carrinho extends Activity {
	
	private static final int ACTIVITY_LISTARARTIGOS=0;
	private static final int ACTIVITY_FAZERLISTA=1;
	private static final int ACTIVITY_LISTARLISTAS=2;
	private static final int ACTIVITY_NOTAS=3;
	
	Button Listar;
	Button CriarLista;
	Button notepad;
	Button Listas;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Toast
        .makeText(this, "Vamos fazer umas comprinhas? :D", Toast.LENGTH_LONG)
        .show();
        
        Listar = (Button) findViewById(R.id.btlistarprod);
        notepad = (Button) findViewById(R.id.btnotas);
        CriarLista = (Button) findViewById(R.id.btgerarlista);
        Listas = (Button) findViewById(R.id.btlistasguardadas);
        
        setListeners();
    
    }
    
    public void setListeners(){
    	
    	Listar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				actividadeListarProdutos();
			}
		});
    	
    	notepad.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				actividadeNotas();
			}
		});
    	
    	CriarLista.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				actividadeCriaLista();
			}
		});
    	
    	Listas.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				actividadeVerListas();
			}
		});

    }
    
    /**
     * Cria a actividade para listar os produtos
     */
    private void actividadeListarProdutos() {
        Intent i = new Intent(this, ListarProdutos2.class);
        startActivityForResult(i, ACTIVITY_LISTARARTIGOS);
    }
    
    private void actividadeNotas() {
        Intent i = new Intent(this, Notepadv3.class);
        startActivityForResult(i, ACTIVITY_NOTAS);
    }
    
    private void actividadeCriaLista() {
        Intent i = new Intent(this, CriarLista.class);
        startActivityForResult(i, ACTIVITY_FAZERLISTA);
    }
    
    private void actividadeVerListas() {
        Intent i = new Intent(this, VerListas.class);
        startActivityForResult(i, ACTIVITY_LISTARLISTAS);
    }
    
}