package pcm.uevora;

import android.content.Context;
//import android.util.Log;
//import android.widget.ImageView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.CheckBox;

public class CheckBoxifiedTextView extends LinearLayout {
     
     private TextView mText;
     private CheckBox mCheckBox;
     private CheckBoxifiedText mCheckBoxText;
     
     public CheckBoxifiedTextView(Context context, CheckBoxifiedText aCheckBoxifiedText) {
          super(context);

          /* First CheckBox and the Text to the right (horizontal),
           * not above and below (vertical) */
          this.setOrientation(HORIZONTAL);
          mCheckBoxText = aCheckBoxifiedText;
          mCheckBox = new CheckBox(context);
          mCheckBox.setPadding(0, 0, 20, 0); // 5px to the right
          
          // Set the initial state of the checkbox.
          mCheckBox.setChecked(aCheckBoxifiedText.getChecked());
          
          mCheckBox.setOnClickListener( new OnClickListener() 
          { 
	           /** 
	           * Check or uncked the current checkbox! 
	           */ 
	           @Override 
	           public void onClick(View v) { 
	                    // Force both the checkbox and our own type to be cehcked or unchecked! 
	                    // It's not needed to check the box again but reusing our functions is not too bad  
	                    setCheckBoxState(getCheckBoxState()); 
	           }
          	}); 
          
          
          /* At first, add the CheckBox to ourself
           * (! we are extending LinearLayout) */
          addView(mCheckBox,  new LinearLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
          
          mText = new TextView(context);
          mText.setText(aCheckBoxifiedText.getText());
          
          mText.setOnClickListener( new OnClickListener() 
          { 
                 /** 
            * Check or uncked the current checkbox! 
            */ 
                @Override 
                public void onClick(View v) { 
                     toggleCheckBoxState(); 
                } 

          });
          
          //mText.setPadding(0, 0, 15, 0);
          addView(mText, new LinearLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
     }

     public void setText(String words) {
          mText.setText(words);
     }
     public String getText() {
         return mText.getText().toString();
    }
     /*public void setCheckBoxState(boolean bool)
     {
    	 mCheckBox.setChecked(mCheckBoxText.getChecked());
    	 mCheckBoxText.setChecked(true);
     }*/
     
     public void toggleCheckBoxState() 
     { 
      setCheckBoxState(!getCheckBoxState()); 
     } 
     
     public void setCheckBoxState(boolean bool) 
     { 
      mCheckBox.setChecked(bool);
      mCheckBoxText.setChecked(bool); 
     } 
     
     public boolean getCheckBoxState() 
     { 
      return mCheckBox.isChecked();
     } 
     
     
}