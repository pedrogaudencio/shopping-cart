package pcm.uevora;


import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Lista extends ListActivity {
    /** Called when the activity is first created. */
	
	private String[] arr;
	
	Button btvoltar;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.listarprodutos2);
        
        btvoltar = (Button) findViewById(R.id.btvoltar);
        
        Bundle extras = getIntent().getExtras();
        int decisao = extras.getInt("opcao");
        
			setListeners();
        
			if(decisao==1){
				arr = getResources().getStringArray(R.array.bebidas);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			
			else
				if(decisao==2){
				arr = getResources().getStringArray(R.array.charcutaria);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==3){
				arr=  getResources().getStringArray(R.array.condimentos);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==4){
				arr=  getResources().getStringArray(R.array.congelados);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==5){
				arr=  getResources().getStringArray(R.array.frutas);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==6){
				arr=  getResources().getStringArray(R.array.higiene);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==7){
				arr=  getResources().getStringArray(R.array.lacticinios);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==8){
				arr=  getResources().getStringArray(R.array.legumes);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==9){
				arr=  getResources().getStringArray(R.array.limpeza);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==10){
				arr=  getResources().getStringArray(R.array.mercearia);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==11){
				arr=  getResources().getStringArray(R.array.padaria);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==12){
				arr=  getResources().getStringArray(R.array.papelaria);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==13){
				arr=  getResources().getStringArray(R.array.peixaria);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==14){
				arr=  getResources().getStringArray(R.array.talho);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			else
				if(decisao==15){
				arr=  getResources().getStringArray(R.array.utilidades);
				setListAdapter(new ArrayAdapter<String>(this, R.layout.listarprodutos2, arr));
			}
			
			setListAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, arr));
    }
    
    	public void setListeners(){
    	
    	btvoltar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_OK);
        	    finish();
			}
		});

    }

}
