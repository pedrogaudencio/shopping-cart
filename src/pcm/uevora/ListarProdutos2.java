/**
 * 
 */
package pcm.uevora;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class ListarProdutos2 extends Activity {
	
	private static final int ACTIVITY_LISTARARTIGOS2=4;
	
	Button btBebidas;
	Button btCharcutaria;
	Button btCondimentos;
	Button btCongelados;
	Button btFrutas;
	Button btHigiene;
	Button btLacticinios;
	Button btleghort;
	Button btLimpeza;
	Button btmercearia;
	Button btpadaria;
	Button btpapelaria;
	Button btpeixaria;
	Button bttalho;
	Button btutilidades;
	Button btvoltar;
	
	Context ctx;
	
	
	public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categoriaslista);
        
        ctx = this;
        
        btBebidas = (Button) findViewById(R.id.btbebidas);
    	btCharcutaria = (Button) findViewById(R.id.btcharcutaria);
    	btCondimentos = (Button) findViewById(R.id.btcondimentos);
    	btCongelados = (Button) findViewById(R.id.btcongelados);
    	btFrutas = (Button) findViewById(R.id.btfruta);
    	btHigiene = (Button) findViewById(R.id.bthigiene);
    	btLacticinios = (Button) findViewById(R.id.btlacticinios);
    	btleghort = (Button) findViewById(R.id.btleghort);
    	btLimpeza = (Button) findViewById(R.id.btlimpeza);
    	btmercearia = (Button) findViewById(R.id.btmercearia);
    	btpadaria = (Button) findViewById(R.id.btpadaria);
    	btpapelaria = (Button) findViewById(R.id.btpapelaria);
    	btpeixaria = (Button) findViewById(R.id.btpeixaria);
    	bttalho = (Button) findViewById(R.id.bttalho);
    	btutilidades = (Button) findViewById(R.id.btutilidades);
    	btvoltar = (Button) findViewById(R.id.btvoltar);
    	
    	setListeners();
    
	}
	
	public void setListeners(){
		
		btBebidas.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 1);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btCharcutaria.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 2);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btCondimentos.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 3);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btCongelados.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 4);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btFrutas.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 5);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btHigiene.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 6);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btLacticinios.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 7);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btleghort.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 8);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btLimpeza.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 9);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btmercearia.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 10);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btpadaria.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 11);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btpapelaria.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 12);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btpeixaria.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 13);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		bttalho.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 14);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		btutilidades.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, Lista.class);
				i.putExtra("opcao", 15);
		        startActivityForResult(i, ACTIVITY_LISTARARTIGOS2);
			}
		});
		
		
    	
		btvoltar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_OK);
        	    finish();
			}
		});
   
    }
	

}
