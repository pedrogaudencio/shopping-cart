package pcm.uevora;


import android.app.ListActivity;
import android.os.Bundle;

public class Checkbox extends ListActivity {
    /** Called when the activity is first created. */
	
	private CheckBoxifiedTextListAdapter checkmercearia;
	private CheckBoxifiedTextListAdapter checkpadaria;
	private CheckBoxifiedTextListAdapter checktalho;
	private CheckBoxifiedTextListAdapter checkcharcutaria;
	private CheckBoxifiedTextListAdapter checkpeixaria;
	private CheckBoxifiedTextListAdapter checklegumes;
	private CheckBoxifiedTextListAdapter checkfrutas;
	private CheckBoxifiedTextListAdapter checklacticinios;
	private CheckBoxifiedTextListAdapter checkbebidas;
	private CheckBoxifiedTextListAdapter checkcongelados;
	private CheckBoxifiedTextListAdapter checkcondimentos;
	private CheckBoxifiedTextListAdapter checkhigiente;
	private CheckBoxifiedTextListAdapter checklimpeza;
	private CheckBoxifiedTextListAdapter checkutilidades;
	private CheckBoxifiedTextListAdapter checkpapelaria;
	// Criar CheckBox List Adapter
	
	private String[] mercearia = {"Arroz", "Azeite", "Batatas fritas", "Caf�", "Cereais", "Ch�", "Chocolate", "Esparguete", "Farinha", "Fermento", "Gelatina", "Massas", "�leo", "Ovos", "Pat�", "Sopa", "Vinagre"};
	private String[] padaria = {"P�o de trigo", "P�o de forma", "Biscoitos", "Bolos"};
	private String[] talho = {"Borrego", "Vaca", "Porco", "Frango", "Leit�o", "Per�", "Pato", "Mi�dos", "Hamburgueres"};
	private String[] charcutaria = {"Fiambre", "Paio", "Bacon", "Mortadela", "Presunto", "Salame", "Lingui�a", "Salsichas"};
	private String[] peixaria = {"Salm�o", "Sardinha", "Bacalhau", "At�m", "Linguado"};
	private String[] legumes = {"Alface", "Agri�o", "Espinafre", "R�cula", "Couve", "Couve-Flor", "Aipo", "Alho frances", "Espargos", "Cogumelos", "Ab�bora", "Beringela", "Pepino", "Piment�o", "Quiabo", "Lentilha", "Gr�o", "Ervilha", "Soja", "Beterraba", "Nabo", "Rabanete", "Hortel�", "Salsa", "Coentros", "Br�culos", "Batata", "Cebola", "Alho", "Cenoura", "Piment�o", "Repolho", "Tomate", "Xuxu", "Feij�o", "Milho"};
	private String[] frutas = {"Abacaxi", "Banana", "Laranja", "Lim�o", "Maracuj�", "Ma��", "Melancia", "Uva", "Manga", "Goiaba", "Mel�o", "C�co", "Ameixa", "Cereja", "Framboesa", "Goiaba", "Kiwi", "Manga", "Morango", "P�ra", "P�ssego", "Tangerina", "Frutos secos"};
	private String[] lacticinios = {"Leite de vaca", "Leite de soja", "Leite Achocolatado", "Manteiga", "Queijo", "Iogurte", "Margarina", "Natas", "Natas de soja", "Chantily"};
	private String[] bebidas = {"Agua Mineral", "Refrigerante", "Sumo", "Cerveja", "Vinho"};
	private String[] congelados = {"Pescada", "Pr�-cozinhados",	"Espinafres congelados", "Hamburgueres congelados", "Douradinhos", "Gelados", "Favas congeladas", "Jardineira congelada", "Bolos congelados", "Redfish congelado", "Camar�o congelado", "Pizza congelada", "Batatas fritas congeladas", "Fruta congelada", "Gelo"};
	private String[] condimentos = {"A�ucar", "Ado�ante", "Caldo carne", "Caldo galinha", "Ervas Arom�ticas", "Ketchup", "Maionese", "Molhos", "Mostarda", "Sal", "Temperos"};
	private String[] higiene = {"�lcool", "Desodorizante", "Champ�", "Condicionador", "Gel de duche", "Sabonete", "Esponja banho", "Creme Hidratante", "Protector Solar", "Papel Higi�nico", "Gaze", "Algod�o", "Fio dental", "Escova de dentes", "Pasta de dentes", "Espuma de barbear", "After-Shave"};
	private String[] limpeza = {"Detergente", "Desengordurante", "Esponja", "Insecticida", "Lava-Loi�a", "Lava-Roupa", "Limpa-Carpetes", "Limpa-Vidros", "Tira N�doas", "Lustra M�veis", "Lustra Piso", "Sacos de Lixo", "Pano de Limpeza", "Pano da Loi�a", "Esfregona", "Vassoura", "Balde"};
	private String[] utilidades = {"Papel absorvente", "Comida para o animal dom�stico", "Fita isoladora", "Graxa", "Fertilizante", "Insecticida", "Molas da roupa", "Papel de alum�nio", "Pelicula aderente", "Lampadas"};
	private String[] papelaria = {"Folhas pautadas", "Folhas quadriculadas", "Papel milim�trico", "Bloco A3", "Separadores", "Dossiers", "Canetas", "Marcadores", "Cartolina", "Livros escolares", "R�gua", "Esquadro", "Compasso", "Borracha", "L�pis de cor"};
	// Array of string we want to display in our list

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.main);
        
        checkmercearia = new CheckBoxifiedTextListAdapter(this);
    	checkpadaria = new CheckBoxifiedTextListAdapter(this);
    	checktalho = new CheckBoxifiedTextListAdapter(this);
    	checkcharcutaria = new CheckBoxifiedTextListAdapter(this);
    	checkpeixaria = new CheckBoxifiedTextListAdapter(this);
    	checklegumes = new CheckBoxifiedTextListAdapter(this);
    	checkfrutas = new CheckBoxifiedTextListAdapter(this);
    	checklacticinios = new CheckBoxifiedTextListAdapter(this);
    	checkbebidas = new CheckBoxifiedTextListAdapter(this);
    	checkcongelados = new CheckBoxifiedTextListAdapter(this);
    	checkcondimentos = new CheckBoxifiedTextListAdapter(this);
    	checkhigiente = new CheckBoxifiedTextListAdapter(this);
    	checklimpeza = new CheckBoxifiedTextListAdapter(this);
    	checkutilidades = new CheckBoxifiedTextListAdapter(this);
    	checkpapelaria = new CheckBoxifiedTextListAdapter(this);
    	
        for(int i=0; i<mercearia.length; i++) {checkmercearia.addItem(new CheckBoxifiedText(mercearia[i], false));}
        for(int i=0; i<padaria.length; i++) {checkpadaria.addItem(new CheckBoxifiedText(padaria[i], false));}
        for(int i=0; i<talho.length; i++) {checktalho.addItem(new CheckBoxifiedText(talho[i], false));}
        for(int i=0; i<charcutaria.length; i++) {checkcharcutaria.addItem(new CheckBoxifiedText(charcutaria[i], false));}
        for(int i=0; i<peixaria.length; i++) {checkpeixaria.addItem(new CheckBoxifiedText(peixaria[i], false));}
        for(int i=0; i<legumes.length; i++) {checklegumes.addItem(new CheckBoxifiedText(legumes[i], false));}
        for(int i=0; i<frutas.length; i++) {checkfrutas.addItem(new CheckBoxifiedText(frutas[i], false));}
        for(int i=0; i<lacticinios.length; i++) {checklacticinios.addItem(new CheckBoxifiedText(lacticinios[i], false));}
        for(int i=0; i<bebidas.length; i++) {checkbebidas.addItem(new CheckBoxifiedText(bebidas[i], false));}
        for(int i=0; i<congelados.length; i++) {checkcongelados.addItem(new CheckBoxifiedText(congelados[i], false));}
        for(int i=0; i<condimentos.length; i++) {checkcondimentos.addItem(new CheckBoxifiedText(condimentos[i], false));}
        for(int i=0; i<higiene.length; i++) {checkhigiente.addItem(new CheckBoxifiedText(higiene[i], false));}
        for(int i=0; i<limpeza.length; i++) {checklimpeza.addItem(new CheckBoxifiedText(limpeza[i], false));}
        for(int i=0; i<utilidades.length; i++) {checkutilidades.addItem(new CheckBoxifiedText(utilidades[i], false));}
        for(int i=0; i<papelaria.length; i++) {checkpapelaria.addItem(new CheckBoxifiedText(papelaria[i], false));}
        // Display it
        setListAdapter(checkmercearia);
        setListAdapter(checkpadaria);
        setListAdapter(checktalho);
        setListAdapter(checkcharcutaria);
        setListAdapter(checkpeixaria);
        setListAdapter(checklegumes);
        setListAdapter(checkfrutas);
        setListAdapter(checklacticinios);
        setListAdapter(checkbebidas);
        setListAdapter(checkcongelados);
        setListAdapter(checkcondimentos);
        setListAdapter(checkhigiente);
        setListAdapter(checklimpeza);
        setListAdapter(checkutilidades);
        setListAdapter(checkpapelaria);
    }
    
    /* Remember the other methods of the CheckBoxifiedTextListAdapter class!!!!
     * cbla.selectAll() :: Check all items in list
     * cbla.deselectAll() :: Uncheck all items
     */
}