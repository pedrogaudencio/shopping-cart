/**
 * 
 */
package pcm.uevora;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pcm.uevora.note.NoteEdit;
import pcm.uevora.note.NotesDbAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Seph
 *
 */

public class CriarLista extends Activity {

	private ListasDbAdapter mDbHelper;
	
	private static final int ACTIVITY_CREATE=0;
	
	private static final int INSERT_ID = Menu.FIRST;
	
	private String [] lista = null;
	
	ListView listview;
	
	EditText tx;
	
	Button btcancel;
	Button btok;
	
	Context ctx; //private
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layoutlistavazia);
        mDbHelper = new ListasDbAdapter(this);
        mDbHelper.open();
        
        listview = (ListView)findViewById(R.id.listaCriar);
        
        tx = (EditText)findViewById(R.id.titulo);
        
        btcancel = (Button) findViewById(R.id.btcancelar);
    	btok = (Button) findViewById(R.id.btok);
        
    	ctx = this;
    	
    	if (lista!=null){
    		
	    	setListeners();
	    	
	        fillData();
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, INSERT_ID, 0, R.string.addToList);
        return true;
    }
    
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch(item.getItemId()) {
        case INSERT_ID:
        	adicionarProdutos();
            return true;
        }
       
        return super.onMenuItemSelected(featureId, item);
    }
    
    private void adicionarProdutos() {
        Intent i = new Intent(this, ListarProdutos.class);
        startActivityForResult(i, ACTIVITY_CREATE);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, 
                                    Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        
        String [] temp = null;
        
        if(resultCode == RESULT_OK){
        	Bundle b = intent.getExtras();
        	temp = b.getStringArray("PRODUTOS");
        	if (lista == null)
        		lista = temp;
        	else
        		lista = concat(temp, lista);
        }
        
        List<String> list = Arrays.asList(lista);
        Set<String> set = new HashSet<String>(list);
        
        String[] result = new String[set.size()];

        set.toArray(result);
        
        lista = result;
        
        Arrays.sort(lista);
        
        for(int b=0;b<lista.length;b++)
        	Log.v("LISTA:",lista[b]);
        
        fillData();
    }
    
    private void fillData(){
    	
    	if (lista == null)
    		setContentView(R.layout.layoutlistavazia);
    	else{
    		
    		ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
    		HashMap<String, String> map = new HashMap<String, String>();
    		
    		for(int i=0;i<lista.length;i++){
    				map = new HashMap<String, String>();
    				map.put("a", lista[i]);
    				mylist.add(map);
    			}
    		
    		setContentView(R.layout.layoutlista);
    		btcancel = (Button) findViewById(R.id.btcancelar);
        	btok = (Button) findViewById(R.id.btok);
        	tx = (EditText)findViewById(R.id.titulo);
        	
    		
    		listview = (ListView)findViewById(R.id.listaCriar);
    		listview.setAdapter(new SimpleAdapter(listview.getContext(),mylist,R.layout.listacriarrow,new String[] {"a"}, new int[] { R.id.listacriarrow_}));
    	
    		setListeners();
    	}
    }
    
    public void setListeners(){
		
		btcancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(-2);
        	    finish();
			}
		});
		
    	btok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				if (lista == null){
					
					Toast
			        .makeText(ctx, "A lista est� vazia", Toast.LENGTH_LONG)
			        .show();
					
					return;
				}
				
				if (tx.getText() == null || tx.getText().toString().equals("")){
					
					Toast
			        .makeText(ctx, "D� um nome � lista primeiro", Toast.LENGTH_LONG)
			        .show();
					
					return;
				}
				
				mDbHelper.open();
				long id = mDbHelper.createLista(tx.getText().toString());
				mDbHelper.createProduto(lista, id);
				mDbHelper.close();
				
				setResult(RESULT_OK);
        	    finish();
			}
		});
   
    }
    
    public String[] concat(String[] A, String[] B) {
    	String[] C= new String[A.length+B.length];
    	System.arraycopy(A, 0, C, 0, A.length);
    	System.arraycopy(B, 0, C, A.length, B.length);

    	return C;
    }
	
}
